$(function(){

	/*
		TODO:
		az oldal betöltésekor lekérni a /rest/countries URL-ről az országok listáját,
		és a város hozzáadása popup <select> elemébe berakni az <option>-öket
	*/
    var listOfCounties =  $.ajax({
		url: '/rest/countries',
		method : 'GET',
		dataType : 'json',
		complete : function(){
						}
	});
    
	$('#add-city-modal').on('click', '#add-city-modal', function(){
		/*
			TODO: valamiért ez az eseménykezelő le sem fut...
		*/
		var cityData = {
                    $('text').val(), $('number').val(), $("#iso :selected").text(); 
                        /*
				TODO: összeszedni az űrlap adatait
			*/
		};
		$.ajax({
			url: '/rest/cities',
			method : 'PUT',
			data : cityData,
			dataType : 'json',
			complete : function(){
				/*
					TODO: Be kellene csukni a popupot
				*/
			}
		});
	});

	/*
		TODO: ország törlés gombok működjenek
	*/

	$('#delete-country').click( function(deleteCountry) {
		var countryIdToDelete = $(this).parents().first().val();
		$.ajax({
			url: 'countries',
			method : 'DELETE',
			data : countryIdToDelete
		});


	});


	/*
		TODO: város törlés gombok működjenek
	*/
	$('#delete-city').click(function(deleteCity) {
		var cityIdToDelete = $(this).parents().first().val();
		$.ajax({
			url: 'cities',
			method : 'DELETE',
			data : cityIdToDelete
		});


	});


});